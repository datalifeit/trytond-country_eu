# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import country


def register():
    Pool.register(
        country.Country,
        module='country_eu', type_='model')
